import QtQuick 2.12
import QtQuick.Controls 2.12
import SddmComponents 2.0
import "SimpleControls" as Simple

Rectangle {

    readonly property color backgroundColor: Qt.rgba(0, 0, 0, 0.4)
    readonly property color hoverBackgroundColor: Qt.rgba(0, 0, 0, 0.6)
    
    width: 640
    height: 480
    
    LayoutMirroring.enabled: Qt.locale().textDirection == Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    TextConstants { id: textConstants }
    
    Connections {
        target: sddm
        
        function onLoginSucceeded() {}
        
        function onLoginFailed() {
            pw_entry.clear()
            pw_entry.focus = true
            
            errorMsgContainer.visible = true
        }
    }
    
    Background {
        anchors.fill: parent
        source: config.background
        fillMode: Image.PreserveAspectCrop
        onStatusChanged: {
            if (status == Image.Error && source != config.defaultBackground) {
                source = config.defaultBackground
            }
        }
    }

    Row {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        spacing: 10

        Simple.ComboBox {
            id: user_entry
            model: userModel
            currentIndex: userModel.lastIndex
            textRole: "realName"
            width: 240
            height: 40
            font.pointSize: 16
            KeyNavigation.backtab: session
            KeyNavigation.tab: pw_entry
        }

        TextField {
            font.pointSize: 14
            id: pw_entry
            color: "white"
            echoMode: TextInput.Password
            focus: true
            placeholderText: textConstants.promptPassword
            width: 240
            height: 40
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 30
                color: pw_entry.activeFocus ? hoverBackgroundColor : backgroundColor
                border.color: "#0fab81"
                border.width: 3
            }
            onAccepted: sddm.login(user_entry.getValue(), pw_entry.text, session.currentIndex)
            KeyNavigation.backtab: user_entry
            KeyNavigation.tab: loginButton
        }

        Simple.Button {
            font.pointSize: 14
            id: loginButton
            text: textConstants.login
            width: 80
            height: 40
            onClicked: sddm.login(user_entry.getValue(), pw_entry.text, session.currentIndex)
            KeyNavigation.backtab: pw_entry
            KeyNavigation.tab: suspend
        }
        
        Rectangle {
            id: errorMsgContainer
            width: 165
            height: loginButton.height
            color: "#000000"
            clip: true
            visible: false
            border.color: "#F44336"
            border.width: 3
            radius: 0
            
            Label {
                anchors.centerIn: parent
                text: textConstants.loginFailed
                width: 165
                color: "white"
                font.bold: true
                font.pointSize: 14
                elide: Qt.locale().textDirection == Qt.RightToLeft ? Text.ElideLeft : Text.ElideRight
                horizontalAlignment: Qt.AlignHCenter
            }
        }

    }

    Row {
        anchors {
            bottom: parent.bottom
            bottomMargin: 10
            right: parent.right
            rightMargin: 10
        }

        spacing: 10
        
        Simple.Icon {
            id: restart
            text: "󰜉"
            onClicked: sddm.reboot()
            visible: true
            width: 40
            height: 40
            font.pointSize: 26
            KeyNavigation.backtab: suspend; KeyNavigation.tab: shutdown
        }
        
        Simple.Icon {
            id: shutdown
            text: "󰐥"
            width: 40
            height: 40
            font.pointSize: 26
            onClicked: sddm.powerOff()
            visible: true
            KeyNavigation.backtab: restart; KeyNavigation.tab: session
        }
    }

    Simple.ComboBox {
        id: session
        anchors {
            left: parent.left
            leftMargin: 10
            top: parent.top
            topMargin: 10
        }
        font.pointSize: 16
        currentIndex: sessionModel.lastIndex
        model: sessionModel
        textRole: "name"
        width: 200
        height: 40
        visible: sessionModel.rowCount() > 1
        KeyNavigation.backtab: shutdown
        KeyNavigation.tab: user_entry
    }

    Rectangle {
        id: timeContainer
        anchors {
            top: parent.top
            right: parent.right
            topMargin: 10
            rightMargin: 10
        }
        border.color: Qt.rgba(1, 1, 1, 0)
        radius: 0
        color: Qt.rgba(0, 0, 0, 0)
        width: timelb.width + 10
        height: session.height

        Label {
            font.pointSize: 16
            id: timelb
            anchors.centerIn: parent
            text: Qt.formatDateTime(new Date(), "[ HH:mm ]")
            color: "white"
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Timer {
        id: timetr
        interval: 500
        repeat: true
        running: true
        onTriggered: {
            timelb.text = Qt.formatDateTime(new Date(), "[ HH:mm ]")
        }
    }
}
