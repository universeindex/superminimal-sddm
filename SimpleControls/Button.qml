import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: control

    contentItem: Text {
        text: control.text
        font: control.font
        color: "white"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: 70
        implicitHeight: 30
        color: control.down ? Qt.rgba(0, 0, 0, 0.6) : Qt.rgba(0, 0, 0, 0.4)
        border.color: "#78f3d2"
        border.width: 3
        radius: 0
    }
}
